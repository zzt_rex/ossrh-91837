package ${package.DomainRest};

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import ${package.Root}.common.model.Result;
import ${package.Root}.common.query.QueryGenerator;
import ${package.Root}.common.utils.CopyUtils;
import ${package.Root}.common.utils.PageUtil;
import ${package.ClientReq}.${client_QryReqDto};
import ${package.ClientResp}.${client_QryListRespDto};
import ${package.ClientResp}.${client_QryRespDto};
import ${package.DomainService}.${table.serviceName};
import ${package.DomainService}.${table.serviceName};
import ${package.ClientReq}.${client_CreateReqDto};
import ${package.ClientReq}.${client_EditReqDto};
import ${package.ClientResp}.${client_CreateRespDto};
import ${package.ClientResp}.${client_EditRespDto};
import ${package.DomainEntity}.${entity};
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.ConvertUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.Arrays;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>


/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Slf4j
@ApiSort(10)
@AllArgsConstructor
@Api(tags="${table.comment!}接口")
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("/${entity_a}")
public class ${table.controllerName} {

    private final ${table.serviceName} ${serviceImplName};

    @ApiOperation("分页列表")
    @ApiOperationSupport(order = 10)
    @GetMapping("/pageList")
    public Result<Page<${client_QryListRespDto}>> queryPageList(${client_QryReqDto} ${client_QryReqDto ? uncap_first},
                                                                @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                                @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        Page<${entity}> pageList = ${serviceImplName}.pageList(${client_QryReqDto ? uncap_first}, pageNo, pageSize);
        Page<${client_QryListRespDto}> respDtoPage = PageUtil.copyIPage(pageList,${client_QryListRespDto}.class);
        return Result.ok(respDtoPage);
    }

    @ApiOperation("列表")
    @ApiOperationSupport(order = 20)
    @GetMapping("/list")
    public Result<List<${client_QryListRespDto}>> queryList(${client_QryReqDto} ${client_QryReqDto ? uncap_first}) {
        List<${entity}> list = ${serviceImplName}.queryList(${client_QryReqDto ? uncap_first});
        return Result.ok(CopyUtils.copy(list, ${client_QryListRespDto}.class));
    }

    @ApiOperation("获取详情")
    @ApiOperationSupport(order = 30)
    @GetMapping("/{id}")
    public Result<${client_QryRespDto}> findById(@PathVariable("id") Long id) {
        ${entity} ${entity ? uncap_first }  = ${serviceImplName}.getById(id);
        if (${entity ? uncap_first } == null) {
            return Result.tip("没有查询到相关信息!");
        }
        ${client_QryRespDto} ${client_QryRespDto ? uncap_first } = new ${client_QryRespDto}();
        BeanUtils.copyProperties(${entity ? uncap_first }, ${client_QryRespDto ? uncap_first } );
        return Result.ok(${client_QryRespDto ? uncap_first });
    }


    @ApiOperationSupport(order = 30)
    @ApiOperation("新增")
    @PostMapping("/add")
    public Result<${client_CreateRespDto}> add(@RequestBody @Validated ${client_CreateReqDto} ${client_CreateReqDto ? uncap_first}) {
        ${entity} ${entity ? uncap_first} = new ${entity}();
        ${client_CreateRespDto} ${client_CreateRespDto ? uncap_first} = new ${client_CreateRespDto}();
        try {
            BeanUtils.copyProperties(${client_CreateReqDto ? uncap_first}, ${entity ? uncap_first});
            boolean saved = ${serviceImplName}.save(${entity ? uncap_first});
            if (saved) {
                BeanUtils.copyProperties(${entity ? uncap_first}, ${client_CreateRespDto ? uncap_first });
                return Result.ok(${client_CreateRespDto ? uncap_first}, "保存成功!");
            }
        } catch (Exception e) {
            log.error("保存失败!");
        }
        return Result.error("保存失败!");
    }

    @ApiOperationSupport(order = 40)
    @ApiOperation("修改")
    @PutMapping("/edit")
    public Result<${client_EditRespDto}> edit(@RequestBody @Validated ${client_EditReqDto} ${client_EditReqDto ? uncap_first}) {
        ${entity} e = ${serviceImplName}.getById(${client_EditReqDto ? uncap_first}.getId());
        if (e == null) {
            return Result.error("未找到对应实体!");
        } else {
            BeanUtils.copyProperties(${client_EditReqDto ? uncap_first}, e);
            boolean ok = ${serviceImplName}.updateById(e);
            if (ok) {
                return Result.ok("修改成功!");
            }
        }
        return Result.error("修改失败!");
    }

    @ApiOperationSupport(order = 50)
    @ApiOperation("通过id删除")
    @DeleteMapping("/{id}")
    public boolean delete(@PathVariable(name = "id") Long id) {
        return ${serviceImplName}.removeById(id);
    }

    @ApiOperationSupport(order = 60)
    @ApiOperation("批量删除")
    @DeleteMapping("/deleteBatch")
    public boolean deleteBatch(@RequestParam(name = "ids") String ids) {
        String[] idArr = ids.split(",");
        return ${serviceImplName}.removeByIds(Arrays.asList((Long[])ConvertUtils.convert(idArr, Long.class)));
    }


}
