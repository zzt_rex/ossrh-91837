package ${package.DomainService};

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import ${package.ClientReq}.${client_QryReqDto};
import ${package.DomainEntity}.${entity};
import ${superServiceClassPackage};
import java.util.List;
/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {

    /**
    * 分页查询
    * @param  ${client_QryReqDto ? uncap_first}
    * @param pageNo
    * @param pageSize
    * @return
    */
    Page<${entity}> pageList(${client_QryReqDto} ${client_QryReqDto ? uncap_first}, Integer pageNo, Integer pageSize);

    /**
    * 列表查询
    * @param  ${client_QryReqDto ? uncap_first}
    * @return
    */
    List<${entity}> queryList(${client_QryReqDto} ${client_QryReqDto ? uncap_first});
}
</#if>
