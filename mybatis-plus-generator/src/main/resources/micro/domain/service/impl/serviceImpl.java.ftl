package ${package.DomainServiceImpl};

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import ${package.ClientReq}.${client_QryReqDto};
import ${package.DomainEntity}.${entity};
import ${package.DomainMapper}.${table.mapperName};
import ${package.DomainService}.${table.serviceName};
import ${superServiceImplClassPackage};
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}> implements ${table.serviceName} {

    @Override
    public Page<${entity}> pageList(${client_QryReqDto} ${client_QryReqDto ? uncap_first}, Integer pageNo, Integer pageSize) {
        ${entity} ${entity_a} = new ${entity}();
        BeanUtils.copyProperties(${client_QryReqDto ? uncap_first}, ${entity_a});
        Page<${entity}> page = new Page<${entity}>(pageNo, pageSize);
        QueryWrapper<${entity}> queryWrapper = new QueryWrapper<>(${entity_a});
        queryWrapper.orderByDesc("create_time");
        Page<${entity}> pageList = this.page(page, queryWrapper);
        log.debug("查询当前页：" + pageList.getCurrent());
        log.debug("查询当前页数量：" + pageList.getSize());
        log.debug("查询结果数量：" + pageList.getRecords().size());
        log.debug("数据总数：" + pageList.getTotal());
        return pageList;
    }

    @Override
    public List<${entity}> queryList(${client_QryReqDto} ${client_QryReqDto ? uncap_first}) {
        ${entity} ${entity_a} = new ${entity}();
        BeanUtils.copyProperties(${client_QryReqDto ? uncap_first}, ${entity_a});
        QueryWrapper<${entity}> queryWrapper = new QueryWrapper<>(${entity_a});
        String ids = ${client_QryReqDto ? uncap_first}.getIds();
        if (StringUtils.isNotBlank(ids)) {
           queryWrapper.in("id", Arrays.asList(ids.split(",")));
        }
        queryWrapper.orderByDesc("create_time");
        return this.list(queryWrapper);
    }
       
}
</#if>
