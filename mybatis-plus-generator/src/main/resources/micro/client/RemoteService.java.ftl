package ${package.Client};

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import ${package.Root}.common.model.Result;
import ${package.ClientFallback}.${client_Fallback};
import ${package.ClientReq}.${client_CreateReqDto};
import ${package.ClientReq}.${client_EditReqDto};
import ${package.ClientResp}.${client_CreateRespDto};
import ${package.ClientResp}.${client_EditRespDto};
import ${package.ClientConstant}.${client_Constant};
import ${package.ClientReq}.${client_QryReqDto};
import ${package.ClientResp}.${client_QryListRespDto};
import ${package.ClientResp}.${client_QryRespDto};
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@FeignClient(name = ${client_Constant}.NAME, path="${entity_a}",fallbackFactory = ${client_Fallback}.class,decode404 = true)
public interface ${client} {


    @GetMapping("/pageList")
    Result<Page<${client_QryListRespDto}>> queryPageList(@SpringQueryMap ${client_QryReqDto} ${client_QryReqDto ? uncap_first},
                                                          @RequestParam(name = "pageNo") Integer pageNo,
                                                          @RequestParam(name = "pageSize") Integer pageSize);

    @GetMapping("/list")
    Result<List<${client_QryListRespDto}>> queryList(@SpringQueryMap ${client_QryReqDto} ${client_QryReqDto ? uncap_first});

    @GetMapping("/{id}")
    Result<${client_QryRespDto}> findById(@PathVariable("id") Long id);

    @PostMapping("/add")
    Result<${client_CreateRespDto}> add(@RequestBody @Validated ${client_CreateReqDto} ${client_CreateReqDto ? uncap_first});

    @PutMapping("/edit")
    Result<${client_EditRespDto}> edit(@RequestBody @Validated ${client_EditReqDto} ${client_EditReqDto ? uncap_first});

    @DeleteMapping("/{id}")
    boolean delete(@PathVariable("id") Long id);

    @DeleteMapping("/deleteBatch")
    boolean deleteBatch(@RequestParam(name = "ids") String ids);
}
