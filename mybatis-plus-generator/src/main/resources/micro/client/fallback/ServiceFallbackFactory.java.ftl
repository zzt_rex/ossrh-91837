package ${package.ClientFallback};

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import ${package.Root}.common.model.Result;
import ${package.Client}.${client};
import ${package.ClientReq}.${client_CreateReqDto};
import ${package.ClientReq}.${client_EditReqDto};
import ${package.ClientResp}.${client_CreateRespDto};
import ${package.ClientResp}.${client_EditRespDto};
import ${package.ClientReq}.${client_QryReqDto};
import ${package.ClientResp}.${client_QryListRespDto};
import ${package.ClientResp}.${client_QryRespDto};
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;

/**
* ${client}降级工场
*/
@Slf4j
public class ${client_Fallback} implements FallbackFactory<${client}> {

    @Override
    public ${client} create(Throwable throwable) {
        return new ${client}() {
            @Override
            public Result<Page<${client_QryListRespDto}>> queryPageList(${client_QryReqDto} ${client_QryReqDto ? uncap_first}, Integer pageNo, Integer pageSize) {
                return Result.error("查询列表为空!");
            }
            @Override
            public Result<List<${client_QryListRespDto}>> queryList(${client_QryReqDto} ${client_QryReqDto ? uncap_first}) {
                return Result.error("查询列表为空!");
            }
            @Override
            public Result<${client_QryRespDto}> findById(Long id) {
                return Result.error("未找到查询实体!");
            }
            @Override
            public Result<${client_CreateRespDto}> add(${client_CreateReqDto} ${client_CreateReqDto ? uncap_first}) {
                 return Result.error("新增失败!");
            }

            @Override
            public Result<${client_EditRespDto}> edit(${client_EditReqDto} ${client_EditReqDto ? uncap_first}) {
                 return Result.error("修改失败!");
            }

            @Override
            public boolean delete(Long id) {
                return false;
            }

            @Override
            public boolean deleteBatch(String ids) {
                return false;
            }
        };
    }
}
