package ${package.AppRest};

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import ${package.Root}.common.utils.PageUtil;
import ${package.Root}.common.model.Result;
import ${package.AppDto}.${app_CreateDto};
import ${package.AppDto}.${app_EditDto};
import ${package.AppDto}.${app_PageListDto};
import ${package.AppService}.${app_iServiceName};
import ${package.AppVo}.${app_PageListVo};
import ${package.Client}.${client};
import ${package.ClientReq}.${client_CreateReqDto};
import ${package.ClientReq}.${client_EditReqDto};
import ${package.ClientReq}.${client_QryReqDto};
import ${package.ClientResp}.${client_CreateRespDto};
import ${package.ClientResp}.${client_QryListRespDto};
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Api(tags = "${table.comment!}接口")
@ApiSort(10)
@AllArgsConstructor
@RestController
@RequestMapping("/${entity_a}")
public class ${table.controllerName} {

    private final ${client} ${client  ? uncap_first };
    private final ${app_iServiceName} ${serviceImplName};

    @ApiOperationSupport(order = 10)
    @ApiOperation("分页查询")
    @GetMapping("/pageList")
    public Result<IPage<${app_PageListVo}>> queryPageList(${app_PageListDto} ${app_PageListDto ? uncap_first},
                                                            @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                            @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize){
        ${client_QryReqDto} ${client_QryReqDto ? uncap_first} = new ${client_QryReqDto}();
        BeanUtils.copyProperties(${app_PageListDto ? uncap_first},${client_QryReqDto ? uncap_first});
        Result<Page<${client_QryListRespDto}>> res = ${client ? uncap_first}.queryPageList(${client_QryReqDto ? uncap_first}, pageNo, pageSize);
        if (res.isSuccess()) {
            Page<${client_QryListRespDto}> page = res.getData();
            IPage<${app_PageListVo}> pageListVo = PageUtil.copyIPage(page, ${app_PageListVo}.class);
            return Result.ok(pageListVo);
        }
        return Result.error(res.getMessage());
    }

    @ApiOperationSupport(order = 20)
    @ApiOperation("导出excel")
    @GetMapping("/exportExcel")
    public void exportExcel(${app_PageListDto} ${app_PageListDto ? uncap_first},
                            String selectionIds,
                            HttpServletRequest request,HttpServletResponse response) throws IOException {
        ${serviceImplName}.exportExcel(${app_PageListDto ? uncap_first},selectionIds,"${table.comment!}信息","Sheet1",request,response);
    }


    /**
    * @param ${app_CreateDto ? uncap_first}
    * @return
    * @功能：新增
    */
    @ApiOperationSupport(order = 30)
    @ApiOperation("新增")
    @PostMapping("/add")
    public Result<Long> add(@RequestBody @Validated ${app_CreateDto} ${app_CreateDto ? uncap_first}) {
        ${client_CreateReqDto} ${client_CreateReqDto ? uncap_first} = new ${client_CreateReqDto}();
        try {
            BeanUtils.copyProperties(${app_CreateDto ? uncap_first}, ${client_CreateReqDto ? uncap_first});
            Result<${client_CreateRespDto}> createResp = ${client ? uncap_first}.add(${client_CreateReqDto ? uncap_first});
            if (createResp.isSuccess() && createResp.getData() != null) {
                return Result.ok(createResp.getData().getId(),"保存成功!");
            }
        } catch (Exception e) {
            log.error("保存失败!");
        }
        return Result.error("保存失败!");
    }

    /**
    * @param ${app_EditDto ? uncap_first}
    * @return
    * @功能：编辑
    */
    @ApiOperationSupport(order = 40)
    @ApiOperation("修改")
    @PutMapping("/edit")
    public Result<?> edit(@RequestBody @Validated ${app_EditDto} ${app_EditDto ? uncap_first}) {
        ${client_EditReqDto} ${client_EditReqDto ? uncap_first} = new ${client_EditReqDto}();
        BeanUtils.copyProperties(${app_EditDto ? uncap_first},${client_EditReqDto ? uncap_first});
        Result<?> editResp = ${client  ? uncap_first }.edit(${client_EditReqDto ? uncap_first});
        String message = editResp.getMessage();
        if (!editResp.isSuccess()) {
            return Result.error(message);
        } else {
            return Result.ok(message);
        }
    }

    /**
    * @param id
    * @return
    * @功能：删除
    */
    @ApiOperationSupport(order = 50)
    @ApiOperation("删除(单个)")
    @DeleteMapping("/{id}")
    public Result<?> delete(@PathVariable("id") Long id) {
        boolean delete = ${client  ? uncap_first }.delete(id);
        if (!delete) {
            return Result.error("删除失败!");
        }
        return Result.ok("删除成功!");
    }

    /**
    * @param ids
    * @return
    * @功能：删除
    */
    @ApiOperationSupport(order = 60)
    @ApiOperation("删除(多个)")
    @DeleteMapping("/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids") String ids) {
        boolean delete = ${client  ? uncap_first }.deleteBatch(ids);
        if (!delete) {
            return Result.error("删除失败!");
        }
        return Result.ok("删除成功!");
    }

}
