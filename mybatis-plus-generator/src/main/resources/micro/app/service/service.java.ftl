package ${package.AppService};

import ${package.AppDto}.${app_PageListDto};

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface ${table.serviceName} {

    /**
    * 导出列表
    * @param ${app_PageListDto ? uncap_first}
    * @param selectionIds 选中id集合
    * @param excelTitle 表格标题
    * @param sheetName sheet页名
    * @param request
    * @param response
    */
    void exportExcel(${app_PageListDto} ${app_PageListDto ? uncap_first}, String selectionIds, String excelTitle, String sheetName,
                     HttpServletRequest request, HttpServletResponse response) throws IOException;
}
