package ${package.AppServiceImpl};

import ${package.Root}.common.constant.CommonConstant;
import ${package.Root}.common.model.Result;
import ${package.Root}.common.utils.ExcelExportUtil;
import ${package.Root}.common.utils.CopyUtils;
import ${package.Root}.common.utils.FileUtils;
import ${package.AppDto}.${app_PageListDto};
import ${package.AppService}.${app_iServiceName};
import ${package.AppVo}.${app_PageListVo};
import ${package.Client}.${client};
import ${package.ClientReq}.${client_QryReqDto};
import ${package.ClientResp}.${client_QryListRespDto};
import com.jshcbd.pbp.common.utils.CopyUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;


import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class ${table.serviceImplName} implements ${table.serviceName} {

    @Resource
    private ${client} ${client ? uncap_first};

    @Override
    public void exportExcel(${app_PageListDto} ${app_PageListDto ? uncap_first}, String selectionIds, String excelTitle, String sheetName, HttpServletRequest request, HttpServletResponse response) throws IOException {
        ${client_QryReqDto} ${client_QryReqDto ? uncap_first} = new ${client_QryReqDto}();
        BeanUtils.copyProperties(${app_PageListDto ? uncap_first},${client_QryReqDto ? uncap_first});
        //有没有选中,没有选中全部导出
        if (StringUtils.isNotBlank(selectionIds)) {
            //远程查询从请求中获得id(单表的主键列名)逗号隔开,构造in条件queryWrapper
            ${client_QryReqDto ? uncap_first}.setIds(selectionIds);
        }
        Result<List<${client_QryListRespDto}>> listResult = ${client ? uncap_first}.queryList(${client_QryReqDto ? uncap_first});
        if (listResult.isSuccess()) {
            List<${client_QryListRespDto}> exportList = listResult.getData();
            if(!CollectionUtils.isEmpty(exportList)){
                List<${app_PageListVo}> exportVoList = CopyUtils.copy(exportList, ${app_PageListVo}.class);
                String fileName = excelTitle+CommonConstant.EXCEL_SUFFIX;
                ExcelExportUtil.exportExcel(exportVoList,${app_PageListVo}.class,excelTitle,sheetName,fileName,request,response);
            }
        }
    }
}
