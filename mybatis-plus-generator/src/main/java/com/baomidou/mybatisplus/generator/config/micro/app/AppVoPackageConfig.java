package com.baomidou.mybatisplus.generator.config.micro.app;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class AppVoPackageConfig {

    private AppVoPackageConfig(){

    }

    /**
     * 父包名
     */
    private String parent = "";

    /**
     * 包名
     */
    private String moduleName = "vo";


    public String getModuleName() {
        return moduleName;
    }

    /**
     * 包配置信息
     *
     * @since 3.5.0
     */
    private final Map<String, String> packageInfo = new HashMap<>();

    /**
     * 父包名
     */
    @NotNull
    public String getParent() {
        if (StringUtils.isNotBlank(parent)) {
            if (StringUtils.isNotBlank(moduleName)) {
                return parent + StringPool.DOT + moduleName;
            }
        } else if (StringUtils.isNotBlank(moduleName)) {
            return moduleName;
        }
        return parent;
    }

    /**
     * 连接父子包名
     *
     * @param subPackage 子包名
     * @return 连接后的包名
     */
    @NotNull
    public String joinPackage(String subPackage) {
        String parent = getParent();
        return StringUtils.isBlank(parent) ? subPackage : (parent + StringPool.DOT + subPackage);
    }

    /**
     * 获取包配置信息
     *
     * @return 包配置信息
     * @since 3.5.0
     */
    @NotNull
    public Map<String, String> getPackageInfo() {
        if (packageInfo.isEmpty()) {
            packageInfo.put(MicroAppConstVal.APP_VO, this.getModuleName());
        }
        return Collections.unmodifiableMap(this.packageInfo);
    }


    public static class Builder extends AppBaseBuilder {

        private final AppVoPackageConfig appVoPackageConfig = new AppVoPackageConfig();

        public Builder(@NotNull AppPackageConfig appPackageConfig){
            super(appPackageConfig);
        }

        public Builder parent(@NotNull String parent) {
            this.appVoPackageConfig.parent = parent;
            return this;
        }

        public Builder moduleName(@NotNull String moduleName) {
            this.appVoPackageConfig.moduleName = moduleName;
            return this;
        }

        public AppVoPackageConfig get() {
            return this.appVoPackageConfig;
        }

        public Builder getBuiler() {
            return this;
        }
    }


}
