package com.baomidou.mybatisplus.generator.config.micro.app;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.config.micro.MicroBaseBuilder;
import com.baomidou.mybatisplus.generator.config.micro.MicroPackageConfig;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class AppPackageConfig {

    private AppPackageConfig(){
    }


    /**
     * 应用层包名
     */
    private String moduleName = "app";


    private final AppRestPackageConfig.Builder appRestPackageConfigBuilder = new AppRestPackageConfig.Builder(this);

    public AppRestPackageConfig.Builder appRestPackageConfigBuilder(){
        return this.appRestPackageConfigBuilder;
    }


    private final AppDtoPackageConfig.Builder appDtoPackageConfigBuilder = new AppDtoPackageConfig.Builder(this);

    public AppDtoPackageConfig.Builder appDtoPackageConfigBuilder(){
        return this.appDtoPackageConfigBuilder;
    }

    private final AppServicePackageConfig.Builder appServicePackageConfigBuilder = new AppServicePackageConfig.Builder(this);

    public AppServicePackageConfig.Builder appServicePackageConfigBuilder(){
        return this.appServicePackageConfigBuilder;
    }


    private final AppVoPackageConfig.Builder appVoPackageConfigBuilder = new AppVoPackageConfig.Builder(this);

    public AppVoPackageConfig.Builder appVoPackageConfigBuilder(){
        return this.appVoPackageConfigBuilder;
    }

    public String getModuleName() {
        return moduleName;
    }


    private void joinPackage(Map<String, String> parentMap, Map<String, String> subPackageInfo) {
        if (CollectionUtils.isNotEmpty(subPackageInfo)
            && StringUtils.isNotBlank(moduleName)) {
            for (String key : subPackageInfo.keySet()) {
                String pageckageName = subPackageInfo.get(key);
                if (StringUtils.isNotBlank(pageckageName)) {
                    parentMap.put(key, moduleName + StringPool.DOT + pageckageName);
                }
            }
        }
    }

    /**
     * 包配置信息
     *
     * @since 3.5.0
     */
    private final Map<String, String> packageInfo = new HashMap<>();

    public Map<String, String> getPackageInfo() {
        if (packageInfo.isEmpty()) {
            packageInfo.put(MicroAppConstVal.APP_MODULE_NAME, this.getModuleName());
            joinPackage(packageInfo, appRestPackageConfigBuilder.get().getPackageInfo());
            joinPackage(packageInfo, appDtoPackageConfigBuilder.get().getPackageInfo());
            joinPackage(packageInfo, appServicePackageConfigBuilder.get().getPackageInfo());
            joinPackage(packageInfo, appVoPackageConfigBuilder.get().getPackageInfo());
        }
        return Collections.unmodifiableMap(this.packageInfo);
    }


    public static class Builder extends MicroBaseBuilder {

        private final AppPackageConfig appPackageConfig = new AppPackageConfig();

        public Builder(@NotNull MicroPackageConfig microPackageConfig) {
            super(microPackageConfig);
        }

        public Builder moduleName(@NotNull String moduleName) {
            this.appPackageConfig.moduleName = moduleName;
            return this;
        }

        public AppPackageConfig get() {
            return this.appPackageConfig;
        }

    }


}
