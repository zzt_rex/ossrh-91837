package com.baomidou.mybatisplus.generator.config.micro.domain;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.config.micro.MicroBaseBuilder;
import com.baomidou.mybatisplus.generator.config.micro.MicroPackageConfig;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DomainPackageConfig {

    private DomainPackageConfig() {

    }

    /**
     * 领域层包名
     */
    private String moduleName = "domain";


    /**
     * Service包名
     */
    private String service = "service";

    /**
     * Service Impl包名
     */
    private String serviceImpl = "service.impl";

    /**
     * Mapper包名
     */
    private String mapper = "mapper";

    /**
     * Mapper XML包名
     */
    private String xml = "mapper.xml";

    /**
     * Controller包名
     */
    private String controller = "rest";

    /**
     * Entity包名
     */
    private String entity = "entity";



    public String getService() {
        return service;
    }

    public String getServiceImpl() {
        return serviceImpl;
    }

    public String getMapper() {
        return mapper;
    }

    public String getXml() {
        return xml;
    }

    public String getController() {
        return controller;
    }

    public String getEntity() {
        return entity;
    }

    public String getModuleName() {
        return moduleName;
    }

    /**
     * 连接父子包名
     *
     * @param subPackage 子包名
     * @return 连接后的包名
     */
    @NotNull
    public String joinPackage(String subPackage) {
        String parent = getModuleName();
        return StringUtils.isBlank(parent) ? subPackage : (parent + StringPool.DOT + subPackage);
    }

    /**
     * 包配置信息
     *
     * @since 3.5.0
     */
    private final Map<String, String> packageInfo = new HashMap<>();

    public Map<String, String> getPackageInfo() {
        if (packageInfo.isEmpty()) {
            packageInfo.put(MicroDomainConstVal.DOMAIN_MODULE_NAME, this.getModuleName());
            packageInfo.put(MicroDomainConstVal.DOMAIN_MAPPER, this.joinPackage(this.getMapper()));
            packageInfo.put(MicroDomainConstVal.DOMAIN_XML, this.joinPackage(this.getXml()));
            packageInfo.put(MicroDomainConstVal.DOMAIN_SERVICE, this.joinPackage(this.getService()));
            packageInfo.put(MicroDomainConstVal.DOMAIN_SERVICE_IMPL, this.joinPackage(this.getServiceImpl()));
            packageInfo.put(MicroDomainConstVal.DOMAIN_REST, this.joinPackage(this.getController()));
            packageInfo.put(MicroDomainConstVal.DOMAIN_ENTITY, this.joinPackage(this.getEntity()));
        }
        return Collections.unmodifiableMap(this.packageInfo);
    }

    public static class Builder extends MicroBaseBuilder {

        private final DomainPackageConfig domainPackageConfig = new DomainPackageConfig();

        public Builder(@NotNull MicroPackageConfig microPackageConfig) {
            super(microPackageConfig);
        }

        public Builder moduleName(@NotNull String moduleName) {
            this.domainPackageConfig.moduleName = moduleName;
            return this;
        }

        public DomainPackageConfig get() {
            return this.domainPackageConfig;
        }

    }
}
