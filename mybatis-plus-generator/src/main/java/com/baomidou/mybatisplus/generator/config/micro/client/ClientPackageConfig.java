package com.baomidou.mybatisplus.generator.config.micro.client;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.config.micro.MicroBaseBuilder;
import com.baomidou.mybatisplus.generator.config.micro.MicroPackageConfig;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ClientPackageConfig {

    private ClientPackageConfig(){
    }


    /**
     * feign包名
     */
    private String moduleName = "client";

    private String fallback = "fallback";

    private String req = "req";

    private String resp = "resp";

    private String constant = "constant";

    public String getConstant() {
        return constant;
    }

    public String getFallback() {
        return fallback;
    }

    public String getReq() {
        return req;
    }

    public String getResp() {
        return resp;
    }


    public String getModuleName() {
        return moduleName;
    }


    /**
     * 连接父子包名
     *
     * @param subPackage 子包名
     * @return 连接后的包名
     */
    @NotNull
    public String joinPackage(String subPackage) {
        String parent = getModuleName();
        return StringUtils.isBlank(parent) ? subPackage : (parent + StringPool.DOT + subPackage);
    }


    /**
     * 包配置信息
     *
     * @since 3.5.0
     */
    private final Map<String, String> packageInfo = new HashMap<>();

    public Map<String, String> getPackageInfo() {
        if (packageInfo.isEmpty()) {
            packageInfo.put(MicroClientConstVal.CLIENT_MODULE_NAME, this.getModuleName());
            packageInfo.put(MicroClientConstVal.CLIENT_FALLBACK, this.joinPackage(this.getFallback()));
            packageInfo.put(MicroClientConstVal.CLIENT_REQ, this.joinPackage(this.getReq()));
            packageInfo.put(MicroClientConstVal.CLIENT_RESP, this.joinPackage(this.getResp()));
            packageInfo.put(MicroClientConstVal.CLIENT_CONSTANT, this.joinPackage(this.getConstant()));
            packageInfo.put(MicroClientConstVal.CLIENT, this.getModuleName());
        }
        return Collections.unmodifiableMap(this.packageInfo);
    }


    public static class Builder extends MicroBaseBuilder {

        private final ClientPackageConfig clientPackageConfig = new ClientPackageConfig();

        public Builder(@NotNull MicroPackageConfig microPackageConfig) {
            super(microPackageConfig);
        }

        public Builder moduleName(@NotNull String moduleName) {
            this.clientPackageConfig.moduleName = moduleName;
            return this;
        }

        public ClientPackageConfig get() {
            return this.clientPackageConfig;
        }

    }


}
