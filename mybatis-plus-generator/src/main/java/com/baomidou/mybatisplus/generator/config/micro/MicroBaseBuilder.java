package com.baomidou.mybatisplus.generator.config.micro;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.config.IConfigBuilder;
import com.baomidou.mybatisplus.generator.config.micro.domain.DomainPackageConfig;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

public class MicroBaseBuilder implements IConfigBuilder<MicroPackageConfig> {
    /**
     * 读取控制台输入内容
     */
    private final Scanner scanner = new Scanner(System.in);

    private final MicroPackageConfig microPackageConfig;

    public MicroBaseBuilder(@NotNull MicroPackageConfig microPackageConfig) {
        this.microPackageConfig = microPackageConfig;
    }


    @NotNull
    public DomainPackageConfig.Builder domainPackageConfigBuilder() {
        return microPackageConfig.domainPackageConfigBuilder();
    }

    /**
     * 包配置
     *
     * @param consumer 自定义包配置
     */
    public MicroBaseBuilder domainPackageConfig(Consumer<DomainPackageConfig.Builder> consumer) {
        consumer.accept(this.microPackageConfig.domainPackageConfigBuilder());
        return this;
    }

    public MicroBaseBuilder domainPackageConfig(BiConsumer<Function<String, String>, DomainPackageConfig.Builder> biConsumer) {
        biConsumer.accept(message -> scannerNext(message),this.microPackageConfig.domainPackageConfigBuilder());
        return this;
    }


    @NotNull
    @Override
    public MicroPackageConfig build() {
        return this.microPackageConfig;
    }


    /**
     * 控制台输入内容读取并打印提示信息
     *
     * @param message 提示信息
     */
    public String scannerNext(String message) {
        System.out.println(message);
        String nextLine = scanner.nextLine();
        if (StringUtils.isBlank(nextLine)) {
            // 如果输入空行继续等待
            return scanner.next();
        }
        return nextLine;
    }

}
