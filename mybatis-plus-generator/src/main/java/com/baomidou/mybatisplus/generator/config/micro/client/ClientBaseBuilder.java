package com.baomidou.mybatisplus.generator.config.micro.client;

import com.baomidou.mybatisplus.generator.config.IConfigBuilder;
import org.jetbrains.annotations.NotNull;

public class ClientBaseBuilder implements IConfigBuilder<ClientPackageConfig> {
    private final ClientPackageConfig clientPackageConfig;

    public ClientBaseBuilder(@NotNull ClientPackageConfig clientPackageConfig) {
        this.clientPackageConfig = clientPackageConfig;
    }


    @NotNull
    @Override
    public ClientPackageConfig build() {
        return this.clientPackageConfig;
    }

}
