/*
 * Copyright (c) 2011-2021, baomidou (jobob@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.baomidou.mybatisplus.generator.config.micro;

/**
 * 定义常量
 *
 * @author YangHu, tangguo, hubin
 * @since 2016-08-31
 */
public interface MicroConstVal {

    String ROOT = "Root";
    String MODULE_NAME = "ModuleName";
    String PARENT = "Parent";

    String TEMPLATE_APP_DTO_CREATE_DTO = "/micro/app/dto/createDto.java";
    String TEMPLATE_APP_DTO_EDIT_DTO = "/micro/app/dto/editDto.java";
    String TEMPLATE_APP_DTO_PAGELIST_DTO = "/micro/app/dto/pageListDto.java";

    String TEMPLATE_APP_CONTROLLER = "/micro/app/rest/controller.java";

    String TEMPLATE_APP_SERVICE = "/micro/app/service/service.java";
    String TEMPLATE_APP_SERVICE_IMPL = "/micro/app/service/impl/serviceImpl.java";

    String TEMPLATE_APP_VO_PAGELIST_VO = "/micro/app/vo/pageListVo.java";

    String TEMPLATE_CLIENT = "/micro/client/RemoteService.java";
    String TEMPLATE_CLIENT_FALLBACK = "/micro/client/fallback/ServiceFallbackFactory.java";
    String TEMPLATE_CLIENT_REQ_CREATE_DTO = "/micro/client/req/CreateReqDto.java";
    String TEMPLATE_CLIENT_REQ_EDIT_DTO = "/micro/client/req/EditReqDto.java";
    String TEMPLATE_CLIENT_REQ_QRY_REQ_DTO = "/micro/client/req/QryReqDto.java";
    String TEMPLATE_CLIENT_RESP_CREATE_DTO = "/micro/client/resp/CreateRespDto.java";
    String TEMPLATE_CLIENT_RESP_EDIT_DTO = "/micro/client/resp/EditRespDto.java";

    String TEMPLATE_CLIENT_CONSTANT = "/micro/client/constant/service.java";

    String TEMPLATE_CLIENT_RESP_QRY_DTO = "/micro/client/resp/QryRespDto.java";
    String TEMPLATE_CLIENT_RESP_QRY_LIST_DTO = "/micro/client/resp/QryListRespDto.java";

    String TEMPLATE_DOMAIN_CONTROLLER = "/micro/domain/rest/controller.java";
    String TEMPLATE_DOMAIN_MAPPER = "/micro/domain/mapper/mapper.java";
    String TEMPLATE_DOMAIN_MAPPER_XML = "/micro/domain/mapper/xml/mapper.xml";
    String TEMPLATE_DOMAIN_SERVICE = "/micro/domain/service/service.java";
    String TEMPLATE_DOMAIN_SERVICE_IMPL = "/micro/domain/service/impl/serviceImpl.java";
    String TEMPLATE_DOMAIN_ENTITY = "/micro/domain/entity/entity.java";



    String VM_LOAD_PATH_KEY = "file.resource.loader.class";
    String VM_LOAD_PATH_VALUE = "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader";

    String SUPER_MAPPER_CLASS = "com.baomidou.mybatisplus.core.mapper.BaseMapper";
    String SUPER_SERVICE_CLASS = "com.baomidou.mybatisplus.extension.service.IService";
    String SUPER_SERVICE_IMPL_CLASS = "com.baomidou.mybatisplus.extension.service.impl.ServiceImpl";

}
