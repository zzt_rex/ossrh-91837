/*
 * Copyright (c) 2011-2021, baomidou (jobob@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.baomidou.mybatisplus.generator.config.builder;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.micro.MicroOutputFile;
import com.baomidou.mybatisplus.generator.config.micro.MicroPackageConfig;
import com.baomidou.mybatisplus.generator.config.micro.MicroTemplateConfig;
import com.baomidou.mybatisplus.generator.config.micro.app.MicroAppConstVal;
import com.baomidou.mybatisplus.generator.config.micro.client.MicroClientConstVal;
import com.baomidou.mybatisplus.generator.config.micro.domain.MicroDomainConstVal;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * 路径信息处理
 *
 * @author nieqiurong hubin
 * @since 2020-10-06
 * @since 3.5.0
 */
class PathInfoHandler {

    /**
     * 输出文件Map
     */
    private final Map<OutputFile, String> pathInfo = new HashMap<>();

    /**
     * 输出文件Map
     */
    private final Map<MicroOutputFile, String> microPathInfo = new HashMap<>();

    /**
     * 输出目录
     */
    private final String outputDir;

    /**
     * 包配置信息
     */
    private final PackageConfig packageConfig;

    /**
     * 包配置信息(微服务)
     */
    private final MicroPackageConfig microPackageConfig;

    PathInfoHandler(GlobalConfig globalConfig, TemplateConfig templateConfig, PackageConfig packageConfig) {
        this.outputDir = globalConfig.getOutputDir();
        this.packageConfig = packageConfig;
        this.microPackageConfig = null;
        // 设置默认输出路径
        this.setDefaultPathInfo(globalConfig, templateConfig);
        // 覆盖自定义路径
        Map<OutputFile, String> pathInfo = packageConfig.getPathInfo();
        if (CollectionUtils.isNotEmpty(pathInfo)) {
            this.pathInfo.putAll(pathInfo);
        }
    }

    PathInfoHandler(GlobalConfig globalConfig, MicroTemplateConfig microTemplateConfig, MicroPackageConfig microPackageConfig) {
        this.outputDir = globalConfig.getOutputDir();
        this.microPackageConfig = microPackageConfig;
        this.packageConfig = null;
        // 设置微服务的默认输出路径
        this.setDefaultPathInfoMicro(globalConfig, microTemplateConfig);
        // 覆盖自定义路径
        Map<MicroOutputFile, String> pathInfo = microPackageConfig.getPathInfo();
        if (CollectionUtils.isNotEmpty(pathInfo)) {
            this.microPathInfo.putAll(pathInfo);
        }
    }

    /**
     * 设置默认输出路径
     *
     * @param globalConfig   全局配置
     * @param templateConfig 模板配置
     */
    private void setDefaultPathInfo(GlobalConfig globalConfig, TemplateConfig templateConfig) {
        putPathInfo(templateConfig.getEntity(globalConfig.isKotlin()), OutputFile.entity, ConstVal.ENTITY);
        putPathInfo(templateConfig.getMapper(), OutputFile.mapper, ConstVal.MAPPER);
        putPathInfo(templateConfig.getXml(), OutputFile.xml, ConstVal.XML);
        putPathInfo(templateConfig.getService(), OutputFile.service, ConstVal.SERVICE);
        putPathInfo(templateConfig.getServiceImpl(), OutputFile.serviceImpl, ConstVal.SERVICE_IMPL);
        putPathInfo(templateConfig.getController(), OutputFile.controller, ConstVal.CONTROLLER);
        putPathInfo(OutputFile.other, ConstVal.OTHER);
    }

    /**
     * 设置默认输出路径
     *
     * @param globalConfig   全局配置
     * @param microTemplateConfig 模板配置
     */
    private void setDefaultPathInfoMicro(GlobalConfig globalConfig, MicroTemplateConfig microTemplateConfig) {

        putPathInfo(microTemplateConfig.getAppController(), MicroOutputFile.appController, MicroAppConstVal.APP_REST);
        putPathInfo(microTemplateConfig.getAppService(), MicroOutputFile.appService, MicroAppConstVal.APP_SERVICE);
        putPathInfo(microTemplateConfig.getAppServiceImpl(), MicroOutputFile.appServiceImpl, MicroAppConstVal.APP_SERVICE_IMPL);
        putPathInfo(microTemplateConfig.getAppDtoCreateDto(), MicroOutputFile.appDtoCreateDto, MicroAppConstVal.APP_DTO);
        putPathInfo(microTemplateConfig.getAppDtoEditDto(), MicroOutputFile.appDtoEditDto, MicroAppConstVal.APP_DTO);
        putPathInfo(microTemplateConfig.getAppDtoPagelistDto(), MicroOutputFile.appDtoPagelistDto, MicroAppConstVal.APP_DTO);
        putPathInfo(microTemplateConfig.getAppVoPagelistVo(), MicroOutputFile.appVoPagelistVo, MicroAppConstVal.APP_VO);

        putPathInfo(microTemplateConfig.getDomainController(), MicroOutputFile.domainController, MicroDomainConstVal.DOMAIN_REST);
        putPathInfo(microTemplateConfig.getDomainMapper(), MicroOutputFile.domainMapper, MicroDomainConstVal.DOMAIN_MAPPER);
        putPathInfo(microTemplateConfig.getDomainMapperXml(), MicroOutputFile.domainMapperXml, MicroDomainConstVal.DOMAIN_XML);
        putPathInfo(microTemplateConfig.getDomainService(), MicroOutputFile.domainService, MicroDomainConstVal.DOMAIN_SERVICE);
        putPathInfo(microTemplateConfig.getDomainServiceImpl(), MicroOutputFile.domainServiceImpl, MicroDomainConstVal.DOMAIN_SERVICE_IMPL);
        putPathInfo(microTemplateConfig.getDomainEntity(), MicroOutputFile.domainEntity, MicroDomainConstVal.DOMAIN_ENTITY);

        putPathInfo(microTemplateConfig.getClient(), MicroOutputFile.client, MicroClientConstVal.CLIENT);
        putPathInfo(microTemplateConfig.getClientConstant(), MicroOutputFile.clientConstant, MicroClientConstVal.CLIENT_CONSTANT);

        putPathInfo(microTemplateConfig.getClientFallback(), MicroOutputFile.clientFallback, MicroClientConstVal.CLIENT_FALLBACK);
        putPathInfo(microTemplateConfig.getClientReqCreateDto(), MicroOutputFile.clientReqCreateDto, MicroClientConstVal.CLIENT_REQ);
        putPathInfo(microTemplateConfig.getClientReqEditDto(), MicroOutputFile.clientReqEditDto, MicroClientConstVal.CLIENT_REQ);
        putPathInfo(microTemplateConfig.getClientRespCreateDto(), MicroOutputFile.clientRespCreateDto, MicroClientConstVal.CLIENT_RESP);
        putPathInfo(microTemplateConfig.getClientRespEditDto(), MicroOutputFile.clientRespEditDto, MicroClientConstVal.CLIENT_RESP);

        putPathInfo(microTemplateConfig.getClientReqQryReqDto(), MicroOutputFile.clientReqQryDto, MicroClientConstVal.CLIENT_REQ);
        putPathInfo(microTemplateConfig.getClientRespQryListDto(), MicroOutputFile.clientRespQryListDto, MicroClientConstVal.CLIENT_RESP);
        putPathInfo(microTemplateConfig.getClientRespQryDto(), MicroOutputFile.clientRespQryDto, MicroClientConstVal.CLIENT_RESP);
    }



    public Map<OutputFile, String> getPathInfo() {
        return this.pathInfo;
    }

    private void putPathInfo(String template, OutputFile outputFile, String module) {
        if (StringUtils.isNotBlank(template)) {
            putPathInfo(outputFile, module);
        }
    }

    private void putPathInfo(OutputFile outputFile, String module) {
        pathInfo.putIfAbsent(outputFile, joinPath(outputDir, packageConfig.getPackageInfo(module)));
    }

    public Map<MicroOutputFile, String> getMicroPathInfo() {
        return this.microPathInfo;
    }

    private void putPathInfo(String template, MicroOutputFile microOutputFile, String module) {
        if (StringUtils.isNotBlank(template)) {
            putPathInfo(microOutputFile, module);
        }
    }

    private void putPathInfo(MicroOutputFile microOutputFile , String module) {
        microPathInfo.putIfAbsent(microOutputFile, joinPath(outputDir, microPackageConfig.getPackageInfo(module)));
    }


    /**
     * 连接路径字符串
     *
     * @param parentDir   路径常量字符串
     * @param packageName 包名
     * @return 连接后的路径
     */
    private String joinPath(String parentDir, String packageName) {
        if (StringUtils.isBlank(parentDir)) {
            parentDir = System.getProperty(ConstVal.JAVA_TMPDIR);
        }
        if (!StringUtils.endsWith(parentDir, File.separator)) {
            parentDir += File.separator;
        }
        packageName = packageName.replaceAll("\\.", StringPool.BACK_SLASH + File.separator);
        return parentDir + packageName;
    }
}
