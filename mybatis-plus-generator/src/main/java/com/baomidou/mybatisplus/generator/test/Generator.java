package com.baomidou.mybatisplus.generator.test;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.FastMicroAutoGenerator;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;
import com.baomidou.mybatisplus.generator.fill.Property;

import java.util.*;

public class Generator {

    public static void main2(String[] args) {
        String location = System.getProperty("user.dir");
        String outPutDir = location + "/tmp" + "/src/main/java";
        String dbUrl = "jdbc:postgresql://172.16.2.160:5432/bid";
        String username = "policy";
        String password = "123456";
        Map<String, Object> map = new HashMap<>();
        map.put("name", "lujun");
        map.put("code", "123");


        FastMicroAutoGenerator.create(dbUrl,username,password)
            // 全局配置
            .globalConfig((scanner, builder) ->
                builder.author(scanner.apply("请输入作者名称？"))
                    .outputDir(outPutDir)
                    .author("lujun")
                    .enableSwagger()
                    .enableMicroService())
            // 包配置
            .microPackageConfig((scanner,builder)->{
                builder.parent("com.jshcbd.pbp")
                    .moduleName("keyword")
                    .build()
                ;
            })
            // 策略配置
            .strategyConfig((scanner, builder) ->
                builder.addInclude(getTables(scanner.apply("请输入表名，多个英文逗号分隔？所有输入 all")))
                    .controllerBuilder().enableRestStyle().enableHyphenStyle()
                    .entityBuilder()
//                  .superClass(BaseEntity.class)
//                  .disableSerialVersionUID()
                    .enableChainModel()
                    .enableLombok()
                    .enableRemoveIsPrefix()
                    .enableTableFieldAnnotation()
                    .enableActiveRecord()
                    .versionColumnName("version")
                    .versionPropertyName("version")
                    .logicDeleteColumnName("del_flag")
                    .logicDeletePropertyName("delFlag")
                    .naming(NamingStrategy.no_change)
                    .columnNaming(NamingStrategy.underline_to_camel)
//                                .addSuperEntityColumns("created_by", "created_time", "updated_by", "updated_time")
//                                .addIgnoreColumns("age")
                    .addTableFills(new Column("create_time", FieldFill.INSERT))
                    .addTableFills(new Property("updateTime", FieldFill.INSERT_UPDATE))
                    .idType(IdType.ASSIGN_ID)
                    .formatFileName("%sEntity")
                    .build())
            .injectionConfig(builder -> builder.customMap(map))
            .templateEngine(new FreemarkerTemplateEngine())
            .execute();
    }

    public static void main(String[] args) {
        String location = System.getProperty("user.dir");
        String outPutDir = location + "/tmp" + "/src/main/java";
        String dbUrl = "jdbc:postgresql://172.16.2.160:5432/bid";
        String username = "policy";
        String password = "123456";
        Map<String, Object> map = new HashMap<>();
        map.put("name", "lujun");
        map.put("code", "123");


        Map<String, String> file = new HashMap<>();
        file.put("test1.txt", "/templates/tes1.ftl");
        file.put("test2.txt", "/templates/tes2.ftl");
        FastAutoGenerator.create(dbUrl,username,password)
            // 全局配置
            .globalConfig((scanner, builder) ->
                builder.author(scanner.apply("请输入作者名称？"))
                    .outputDir(outPutDir)
                    .enableSwagger())
            // 包配置
            .packageConfig((scanner, builder) -> builder
                .parent(scanner.apply("请输入包名？"))
                .moduleName(scanner.apply("请输入模块名？")))
            // 策略配置
            .strategyConfig((scanner, builder) ->
                builder.addInclude(getTables(scanner.apply("请输入表名，多个英文逗号分隔？所有输入 all")))
                    .controllerBuilder().enableRestStyle().enableHyphenStyle()
                    .entityBuilder()
//                                .superClass(BaseEntity.class)
//                                .disableSerialVersionUID()
                    .enableChainModel()
                    .enableLombok()
                    .enableRemoveIsPrefix()
                    .enableTableFieldAnnotation()
                    .enableActiveRecord()
                    .versionColumnName("version")
                    .versionPropertyName("version")
                    .logicDeleteColumnName("del_flag")
                    .logicDeletePropertyName("delFlag")
                    .naming(NamingStrategy.no_change)
                    .columnNaming(NamingStrategy.underline_to_camel)
//                                .addSuperEntityColumns("created_by", "created_time", "updated_by", "updated_time")
//                                .addIgnoreColumns("age")
                    .addTableFills(new Column("create_time", FieldFill.INSERT))
                    .addTableFills(new Property("updateTime", FieldFill.INSERT_UPDATE))
                    .idType(IdType.ASSIGN_ID)
                    .formatFileName("%sEntity")
                    .build())
            .injectionConfig(builder -> builder.customMap(map).customFile(file))
            .templateEngine(new FreemarkerTemplateEngine())
            .execute();
    }

    // 处理 all 情况
    protected static List<String> getTables(String tables) {
        return "all".equals(tables) ? Collections.emptyList() : Arrays.asList(tables.split(","));
    }
}
