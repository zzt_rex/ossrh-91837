/*
 * Copyright (c) 2011-2021, baomidou (jobob@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.baomidou.mybatisplus.generator.config.micro;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.config.IConfigBuilder;
import com.baomidou.mybatisplus.generator.config.TemplateType;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 模板路径配置项
 *
 * @author tzg hubin
 * @since 2017-06-17
 */
public class MicroTemplateConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(MicroTemplateConfig.class);


    private String appDtoCreateDto ;
    private String appDtoEditDto ;
    private String appDtoPagelistDto ;

    private String appController ;

    private String appService ;
    private String appServiceImpl ;

    private String appVoPagelistVo;

    private String client;
    private String clientFallback;
    private String clientReqCreateDto;
    private String clientReqEditDto;
    private String clientRespCreateDto;
    private String clientRespEditDto;

    private String clientConstant;

    private String domainController;
    private String domainMapper;
    private String domainMapperXml;
    private String domainService;
    private String domainServiceImpl;

    private String domainEntity;

    private String clientReqQryReqDto;
    private String clientRespQryDto;
    private String clientRespQryListDto;

    /**
     * 不对外暴露
     */
    private MicroTemplateConfig() {
        this.appDtoCreateDto = MicroConstVal.TEMPLATE_APP_DTO_CREATE_DTO;
        this.appDtoEditDto = MicroConstVal.TEMPLATE_APP_DTO_EDIT_DTO;
        this.appDtoPagelistDto = MicroConstVal.TEMPLATE_APP_DTO_PAGELIST_DTO;
        this.appController = MicroConstVal.TEMPLATE_APP_CONTROLLER;
        this.appService = MicroConstVal.TEMPLATE_APP_SERVICE;
        this.appServiceImpl = MicroConstVal.TEMPLATE_APP_SERVICE_IMPL;
        this.appVoPagelistVo = MicroConstVal.TEMPLATE_APP_VO_PAGELIST_VO;
        this.client = MicroConstVal.TEMPLATE_CLIENT;
        this.clientFallback = MicroConstVal.TEMPLATE_CLIENT_FALLBACK;
        this.clientReqCreateDto = MicroConstVal.TEMPLATE_CLIENT_REQ_CREATE_DTO;
        this.clientReqEditDto = MicroConstVal.TEMPLATE_CLIENT_REQ_EDIT_DTO;
        this.clientRespCreateDto = MicroConstVal.TEMPLATE_CLIENT_RESP_CREATE_DTO;
        this.clientRespEditDto = MicroConstVal.TEMPLATE_CLIENT_RESP_EDIT_DTO;
        this.clientReqQryReqDto = MicroConstVal.TEMPLATE_CLIENT_REQ_QRY_REQ_DTO;
        this.clientRespQryDto =  MicroConstVal.TEMPLATE_CLIENT_RESP_QRY_DTO;
        this.clientRespQryListDto = MicroConstVal.TEMPLATE_CLIENT_RESP_QRY_LIST_DTO;

        this.clientConstant = MicroConstVal.TEMPLATE_CLIENT_CONSTANT;

        this.domainController = MicroConstVal.TEMPLATE_DOMAIN_CONTROLLER;
        this.domainMapper = MicroConstVal.TEMPLATE_DOMAIN_MAPPER;
        this.domainMapperXml = MicroConstVal.TEMPLATE_DOMAIN_MAPPER_XML;
        this.domainService = MicroConstVal.TEMPLATE_DOMAIN_SERVICE;
        this.domainServiceImpl = MicroConstVal.TEMPLATE_DOMAIN_SERVICE_IMPL;
        this.domainEntity = MicroConstVal.TEMPLATE_DOMAIN_ENTITY;
    }

    /**
     * 当模板赋值为空时进行日志提示打印
     *
     * @param value        模板值
     * @param templateType 模板类型
     */
    private void logger(String value, TemplateType templateType) {
        if (StringUtils.isBlank(value)) {
            LOGGER.warn("推荐使用disable(TemplateType.{})方法进行默认模板禁用.", templateType.name());
        }
    }



    /**
     * 禁用模板
     *
     * @param templateTypes 模板类型
     * @return this
     * @since 3.3.2
     */
    public MicroTemplateConfig disable(@NotNull MicroTemplateType... templateTypes) {
        if (templateTypes != null && templateTypes.length > 0) {
            for (MicroTemplateType templateType : templateTypes) {
                switch (templateType) {
                    case APP:
                        this.appDtoCreateDto = null;
                        this.appDtoEditDto = null;
                        this.appDtoPagelistDto = null;
                        this.appController = null;
                        this.appService = null;
                        this.appServiceImpl = null;
                        this.appVoPagelistVo = null;
                        break;
                    case APP_DTO:
                        this.appDtoCreateDto = null;
                        this.appDtoEditDto = null;
                        this.appDtoPagelistDto = null;
                        break;
                    case APP_CONTROLLER:
                        this.appController = null;
                        break;
                    case APP_SERVICE:
                        this.appService = null;
                        break;
                    case APP_SERVICE_IMPL:
                        this.appServiceImpl = null;
                        break;
                    case APP_VO:
                        this.appVoPagelistVo = null;
                        break;
                    case CLIENT:
                        this.client = null;
                        this.clientFallback = null;
                        this.clientReqCreateDto = null;
                        this.clientReqEditDto = null;
                        this.clientRespCreateDto = null;
                        this.clientRespEditDto = null;
                        this.clientConstant = null;
                    case CLIENT_FALLBACK:
                        this.clientFallback = null;
                        break;
                    case CLIENT_REQ:
                        this.clientReqCreateDto = null;
                        this.clientReqEditDto = null;
                        break;
                    case CLIENT_RESP:
                        this.clientRespCreateDto = null;
                        this.clientRespEditDto = null;
                        break;
                    case CLIENT_CONSTANT:
                        this.clientConstant = null;
                        break;
                    case DOMAIN:
                        this.domainController = null;
                        this.domainMapper = null;
                        this.domainMapperXml = null;
                        this.domainService = null;
                        this.domainServiceImpl = null;
                        this.domainEntity = null;
                        break;
                    case DOMAIN_CONTROLLER:
                        this.domainController = null;
                        break;
                    case DOMAIN_MAPPER:
                        this.domainMapper = null;
                        break;
                    case DOMAIN_MAPPER_XML:
                        this.domainMapperXml = null;
                        break;
                    case DOMAIN_SERVICE:
                        this.domainService = null;
                        break;
                    case DOMAIN_SERVICE_IMPL:
                        this.domainServiceImpl = null;
                        break;
                    case DOMIAN_ENTITY:
                        this.domainEntity = null;
                        break;
                    default:
                }
            }
        }
        return this;
    }

    public String getAppDtoCreateDto() {
        return appDtoCreateDto;
    }

    public String getAppDtoEditDto() {
        return appDtoEditDto;
    }

    public String getAppDtoPagelistDto() {
        return appDtoPagelistDto;
    }

    public String getAppController() {
        return appController;
    }

    public String getAppService() {
        return appService;
    }

    public String getAppServiceImpl() {
        return appServiceImpl;
    }


    public String getAppVoPagelistVo() {
        return appVoPagelistVo;
    }

    public String getClient() {
        return client;
    }

    public String getClientFallback() {
        return clientFallback;
    }

    public String getClientReqCreateDto() {
        return clientReqCreateDto;
    }

    public String getClientReqEditDto() {
        return clientReqEditDto;
    }

    public String getClientRespCreateDto() {
        return clientRespCreateDto;
    }

    public String getClientRespEditDto() {
        return clientRespEditDto;
    }

    public String getClientConstant() {
        return clientConstant;
    }


    public String getDomainController() {
        return domainController;
    }

    public String getDomainMapper() {
        return domainMapper;
    }

    public String getDomainMapperXml() {
        return domainMapperXml;
    }

    public String getDomainService() {
        return domainService;
    }

    public String getDomainServiceImpl() {
        return domainServiceImpl;
    }

    public String getDomainEntity() {
        return domainEntity;
    }

    public String getClientReqQryReqDto() {
        return clientReqQryReqDto;
    }

    public String getClientRespQryDto() {
        return clientRespQryDto;
    }

    public String getClientRespQryListDto() {
        return clientRespQryListDto;
    }

    /**
     * 禁用全部模板
     *
     * @return this
     * @since 3.5.0
     */
    public MicroTemplateConfig disable() {
        return disable(MicroTemplateType.values());
    }


    /**
     * 模板路径配置构建者
     *
     * @author nieqiurong 3.5.0
     */
    public static class Builder implements IConfigBuilder<MicroTemplateConfig> {

        private final MicroTemplateConfig templateConfig;

        /**
         * 默认生成一个空的
         */
        public Builder() {
            this.templateConfig = new MicroTemplateConfig();
        }

        /**
         * 禁用所有模板
         *
         * @return this
         */
        public Builder disable() {
            this.templateConfig.disable();
            return this;
        }

        /**
         * 禁用模板
         *
         * @return this
         */
        public Builder disable(@NotNull MicroTemplateType... templateTypes) {
            this.templateConfig.disable(templateTypes);
            return this;
        }

        public Builder appDtoCreateDto(@NotNull String appDtoCreateDto){
            this.templateConfig.appDtoCreateDto = appDtoCreateDto;
            return this;
        }
        public Builder appDtoEditDto(@NotNull String appDtoEditDto){
            this.templateConfig.appDtoEditDto = appDtoEditDto;
            return this;
        }
        public Builder appDtoPagelistDto(@NotNull String appDtoPagelistDto){
            this.templateConfig.appDtoPagelistDto = appDtoPagelistDto;
            return this;
        }
        public Builder appController(@NotNull String appController){
            this.templateConfig.appController = appController;
            return this;
        }
        public Builder appService(@NotNull String appService){
            this.templateConfig.appService = appService;
            return this;
        }
        public Builder appServiceImpl(@NotNull String appServiceImpl){
            this.templateConfig.appServiceImpl = appServiceImpl;
            return this;
        }
        public Builder appVoPagelistVo(@NotNull String appVoPagelistVo){
            this.templateConfig.appVoPagelistVo = appVoPagelistVo;
            return this;
        }
        public Builder clientFallback(@NotNull String clientFallback){
            this.templateConfig.clientFallback = clientFallback;
            return this;
        }
        public Builder clientReqCreateDto(@NotNull String clientReqCreateDto){
            this.templateConfig.clientReqCreateDto = clientReqCreateDto;
            return this;
        }
        public Builder clientReqEditDto(@NotNull String clientReqEditDto){
            this.templateConfig.clientReqEditDto = clientReqEditDto;
            return this;
        }
        public Builder clientRespCreateDto(@NotNull String clientRespCreateDto){
            this.templateConfig.clientRespCreateDto = clientRespCreateDto;
            return this;
        }
        public Builder clientRespEditDto(@NotNull String clientRespEditDto){
            this.templateConfig.clientRespEditDto = clientRespEditDto;
            return this;
        }
        public Builder clientConstant(@NotNull String clientConstant){
            this.templateConfig.clientConstant = clientConstant;
            return this;
        }
        public Builder domainController(@NotNull String domainController){
            this.templateConfig.domainController = domainController;
            return this;
        }
        public Builder domainMapper(@NotNull String domainMapper){
            this.templateConfig.domainMapper = domainMapper;
            return this;
        }
        public Builder domainMapperXml(@NotNull String domainMapperXml){
            this.templateConfig.domainMapperXml = domainMapperXml;
            return this;
        }
        public Builder domainService(@NotNull String domainService){
            this.templateConfig.domainService = domainService;
            return this;
        }
        public Builder domainServiceImpl(@NotNull String domainServiceImpl){
            this.templateConfig.domainServiceImpl = domainServiceImpl;
            return this;
        }
        public Builder domainEntity(@NotNull String domainEntity){
            this.templateConfig.domainEntity = domainEntity;
            return this;
        }

        public Builder clientReqQryReqDto(@NotNull String clientReqQryReqDto){
            this.templateConfig.clientReqQryReqDto = clientReqQryReqDto;
            return this;
        }
        public Builder clientRespQryDto(@NotNull String clientRespQryDto){
            this.templateConfig.clientRespQryDto = clientRespQryDto;
            return this;
        }
        public Builder clientRespQryListDto(@NotNull String clientRespQryListDto){
            this.templateConfig.clientRespQryListDto = clientRespQryListDto;
            return this;
        }

        /**
         * 构建模板配置对象
         *
         * @return 模板配置对象
         */
        @Override
        public MicroTemplateConfig build() {
            return this.templateConfig;
        }
    }
}
