/*
 * Copyright (c) 2011-2021, baomidou (jobob@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.baomidou.mybatisplus.generator.config.micro.client;

/**
 * 定义常量
 *
 * @author YangHu, tangguo, hubin
 * @since 2016-08-31
 */
public interface MicroClientConstVal {

    String CLIENT_PARENT = "Client_Parent";

    String CLIENT_MODULE_NAME = "ClientModuleName";

    String CLIENT = "Client";
    String CLIENT_FALLBACK = "ClientFallback";
    String CLIENT_REQ = "ClientReq";
    String CLIENT_RESP = "ClientResp";
    String CLIENT_CONSTANT = "ClientConstant";



}
