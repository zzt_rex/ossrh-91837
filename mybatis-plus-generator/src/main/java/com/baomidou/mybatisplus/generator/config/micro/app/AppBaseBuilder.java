package com.baomidou.mybatisplus.generator.config.micro.app;

import com.baomidou.mybatisplus.generator.config.IConfigBuilder;
import org.jetbrains.annotations.NotNull;

public class AppBaseBuilder implements IConfigBuilder<AppPackageConfig> {
    private final AppPackageConfig appPackageConfig;

    public AppBaseBuilder(@NotNull AppPackageConfig appPackageConfig) {
        this.appPackageConfig = appPackageConfig;
    }


    @NotNull
    @Override
    public AppPackageConfig build() {
        return this.appPackageConfig;
    }

}
