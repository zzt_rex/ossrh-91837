/*
 * Copyright (c) 2011-2021, baomidou (jobob@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.baomidou.mybatisplus.generator.engine;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.builder.ConfigBuilder;
import com.baomidou.mybatisplus.generator.config.micro.MicroOutputFile;
import com.baomidou.mybatisplus.generator.config.micro.MicroTemplateConfig;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.util.FileUtils;
import com.baomidou.mybatisplus.generator.util.RuntimeUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;


/**
 * 模板引擎抽象类
 *
 * @author hubin
 * @since 2018-01-10
 */
public abstract class AbstractTemplateEngine {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 配置信息
     */
    private ConfigBuilder configBuilder;

    /**
     * 模板引擎初始化
     */
    @NotNull
    public abstract AbstractTemplateEngine init(@NotNull ConfigBuilder configBuilder);

    /**
     * 输出自定义模板文件
     *
     * @param customFile 自定义配置模板文件信息
     * @param tableInfo  表信息
     * @param objectMap  渲染数据
     * @since 3.5.1
     */
    protected void outputCustomFile(@NotNull Map<String, String> customFile, @NotNull TableInfo tableInfo, @NotNull Map<String, Object> objectMap) {
        String entityName = tableInfo.getEntityName();
        String otherPath = getPathInfo(OutputFile.other);
        customFile.forEach((key, value) -> {
            String fileName = String.format((otherPath + File.separator + entityName + File.separator + "%s"), key);
            outputFile(new File(fileName), objectMap, value, getConfigBuilder().getInjectionConfig().isFileOverride());
        });
    }


    /**
     * 输出domain领域层文件
     *
     * @param tableInfo 表信息
     * @param objectMap 渲染数据
     * @since 3.5.0
     */
    protected void outputDomain(@NotNull TableInfo tableInfo, @NotNull Map<String, Object> objectMap) {
        String entityName = tableInfo.getEntityName();
        String entityPath = getMicroPathInfo(MicroOutputFile.domainEntity);
        String servicePath = getMicroPathInfo(MicroOutputFile.domainService);
        String serviceImplPath = getMicroPathInfo(MicroOutputFile.domainServiceImpl);
        String mapperPath = getMicroPathInfo(MicroOutputFile.domainMapper);
        String mapperXmlPath = getMicroPathInfo(MicroOutputFile.domainMapperXml);
        String controllerPath = getMicroPathInfo(MicroOutputFile.domainController);
        if (StringUtils.isNotBlank(entityName) && StringUtils.isNotBlank(entityPath)) {
            getMicroTemplateFilePath(MicroTemplateConfig::getDomainEntity).ifPresent((entity) -> {
                String entityFile = String.format((entityPath + File.separator + "%s" + suffixJavaOrKt()), entityName);
                outputFile(new File(entityFile), objectMap, entity, getConfigBuilder().getStrategyConfig().entity().isFileOverride());
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getDomainController).ifPresent((controller) -> {
                String controllerFile = String.format((controllerPath + File.separator + tableInfo.getControllerName() + suffixJavaOrKt()), entityName);
                outputFile(new File(controllerFile), objectMap, controller, getConfigBuilder().getStrategyConfig().controller().isFileOverride());
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getDomainMapper).ifPresent((mapper) -> {
                String mapperFile = String.format((mapperPath + File.separator + tableInfo.getMapperName() + suffixJavaOrKt()), entityName);
                outputFile(new File(mapperFile), objectMap, mapper, getConfigBuilder().getStrategyConfig().mapper().isFileOverride());
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getDomainMapperXml).ifPresent((mapperXml) -> {
                String mapperXmlFile = String.format((mapperXmlPath + File.separator + tableInfo.getXmlName() + ConstVal.XML_SUFFIX), entityName);
                outputFile(new File(mapperXmlFile), objectMap, mapperXml, getConfigBuilder().getStrategyConfig().mapper().isFileOverride());
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getDomainService).ifPresent((service) -> {
                String serviceFile = String.format((servicePath + File.separator + tableInfo.getServiceName() + suffixJavaOrKt()), entityName);
                outputFile(new File(serviceFile), objectMap, service, getConfigBuilder().getStrategyConfig().service().isFileOverride());
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getDomainServiceImpl).ifPresent((serviceImpl) -> {
                String serviceImplFile = String.format((serviceImplPath + File.separator + tableInfo.getServiceImplName() + suffixJavaOrKt()), entityName);
                outputFile(new File(serviceImplFile), objectMap, serviceImpl, getConfigBuilder().getStrategyConfig().service().isFileOverride());
            });
        }
    }

    /**
     * 输出client客戶端文件
     *
     * @param tableInfo 表信息
     * @param objectMap 渲染数据
     * @since 3.5.0
     */
    protected void outputClient(@NotNull TableInfo tableInfo, @NotNull Map<String, Object> objectMap) {
        String entityName = tableInfo.getEntityName();
        String entityA = (String)objectMap.get("entity_A");
        String constantPath = getMicroPathInfo(MicroOutputFile.clientConstant);

        String path = getMicroPathInfo(MicroOutputFile.client);
        String fallbackPath = getMicroPathInfo(MicroOutputFile.clientFallback);
        String reqEditPath = getMicroPathInfo(MicroOutputFile.clientReqEditDto);
        String reqCreatePath = getMicroPathInfo(MicroOutputFile.clientReqCreateDto);
        String respEditPath = getMicroPathInfo(MicroOutputFile.clientRespEditDto);
        String respCreatePath = getMicroPathInfo(MicroOutputFile.clientRespCreateDto);


        String queryReqPath = getMicroPathInfo(MicroOutputFile.clientReqQryDto);
        String queryRespQryPath = getMicroPathInfo(MicroOutputFile.clientRespQryDto);
        String queryRespQryListPath = getMicroPathInfo(MicroOutputFile.clientRespQryListDto);

        if (StringUtils.isNotBlank(entityA) ) {
            getMicroTemplateFilePath(MicroTemplateConfig::getClientConstant).ifPresent((constant) -> {
                String clientConstant = "Service";
                objectMap.put("client_Constant", clientConstant);
                String constantFile = String.format((constantPath + File.separator + "%s" + suffixJavaOrKt()), clientConstant);
                outputFile(new File(constantFile), objectMap, constant, true);
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getClientReqEditDto).ifPresent((reqEdit) -> {
                String clientReqEdit = entityA+"EditReqDTO";
                objectMap.put("client_EditReqDto", clientReqEdit);
                String reqEditFile = String.format((reqEditPath + File.separator +  "%s" + suffixJavaOrKt()),clientReqEdit );
                outputFile(new File(reqEditFile), objectMap, reqEdit,true);
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getClientReqCreateDto).ifPresent((reqCreate) -> {
                String clientCreateReqDto = entityA+"CreateReqDTO";
                objectMap.put("client_CreateReqDto", clientCreateReqDto);
                String reqCreateFile = String.format((reqCreatePath + File.separator + "%s" +  suffixJavaOrKt()),clientCreateReqDto );
                outputFile(new File(reqCreateFile), objectMap, reqCreate, true);
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getClientRespCreateDto).ifPresent((respCreate) -> {
                String clientCreateRespDto = entityA+"CreateRespDTO";
                objectMap.put("client_CreateRespDto", clientCreateRespDto);
                String respCreateFile = String.format((respCreatePath + File.separator + "%s" +  suffixJavaOrKt()),clientCreateRespDto);
                outputFile(new File(respCreateFile), objectMap, respCreate,true);
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getClientRespEditDto).ifPresent((respEdit) -> {
                String clientEditRespDto = entityA+"EditRespDTO";
                objectMap.put("client_EditRespDto", clientEditRespDto);
                String respEditFile = String.format((respEditPath + File.separator + "%s" +  suffixJavaOrKt()), clientEditRespDto);
                outputFile(new File(respEditFile), objectMap, respEdit, true);
            });

            getMicroTemplateFilePath(MicroTemplateConfig::getClientReqQryReqDto).ifPresent((queryReq) -> {
                String clientQryReqDto =  entityA+"QryReqDTO";
                objectMap.put("client_QryReqDto", clientQryReqDto);
                String queryReqFile = String.format((queryReqPath + File.separator +  "%s" + suffixJavaOrKt()),clientQryReqDto);
                outputFile(new File(queryReqFile), objectMap, queryReq,true);
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getClientRespQryDto).ifPresent((qryRespDto) -> {
                String clientQryRespDto =  entityA+"QryRespDTO";
                objectMap.put("client_QryRespDto", clientQryRespDto);
                String qryRespDtoFile = String.format((queryRespQryPath + File.separator + "%s" +  suffixJavaOrKt()),clientQryRespDto);
                outputFile(new File(qryRespDtoFile), objectMap, qryRespDto, true);
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getClientRespQryListDto).ifPresent((qryListResp) -> {
                String clientQryListRespDto =  entityA+"QryListRespDTO";
                objectMap.put("client_QryListRespDto", clientQryListRespDto);
                String qryListRespFile = String.format((queryRespQryListPath + File.separator + "%s" +  suffixJavaOrKt()),clientQryListRespDto );
                outputFile(new File(qryListRespFile), objectMap, qryListResp,true);
            });

            String clientFallback = entityA+"ServiceFallbackFactory";
            objectMap.put("client_Fallback", clientFallback);
            String client = "Remote"+entityA+"Service";
            objectMap.put("client", client);
            getMicroTemplateFilePath(MicroTemplateConfig::getClientFallback).ifPresent((cmdFallback) -> {
                String cmdFallbackFile = String.format((fallbackPath + File.separator +  "%s" + suffixJavaOrKt()), clientFallback);
                outputFile(new File(cmdFallbackFile), objectMap, cmdFallback, true);
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getClient).ifPresent((cmd) -> {
                String cmdServiceFile = String.format((path + File.separator +  "%s" + suffixJavaOrKt()), client);
                outputFile(new File(cmdServiceFile), objectMap, cmd, true);
            });
        }
    }

    /**
     * 输出app应用层文件
     *
     * @param tableInfo 表信息
     * @param objectMap 渲染数据
     * @since 3.5.0
     */
    protected void outputApp(@NotNull TableInfo tableInfo, @NotNull Map<String, Object> objectMap) {
        String entityName = tableInfo.getEntityName();
        String entityA = (String)objectMap.get("entity_A");
        String restPath = getMicroPathInfo(MicroOutputFile.appController);

        String createDtoPath = getMicroPathInfo(MicroOutputFile.appDtoCreateDto);
        String editDtoPath = getMicroPathInfo(MicroOutputFile.appDtoEditDto);
        String pageListDtoPath = getMicroPathInfo(MicroOutputFile.appDtoPagelistDto);

        String servicePath = getMicroPathInfo(MicroOutputFile.appService);
        String serviceImplPath = getMicroPathInfo(MicroOutputFile.appServiceImpl);

        String creataVoPath = getMicroPathInfo(MicroOutputFile.appVoCreateVo);
        String pageListVoPath = getMicroPathInfo(MicroOutputFile.appVoPagelistVo);
        if (StringUtils.isNotBlank(entityA)) {
            getMicroTemplateFilePath(MicroTemplateConfig::getAppDtoCreateDto).ifPresent((createDto) -> {
                String appCreataDto = entityA+"CreateDTO";
                objectMap.put("app_CreateDto", appCreataDto);
                String appCreataDtoFile = String.format((createDtoPath + File.separator +  "%s" + suffixJavaOrKt()),appCreataDto );
                outputFile(new File(appCreataDtoFile), objectMap, createDto,true);
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getAppDtoEditDto).ifPresent((editDto) -> {
                String appEditDto = entityA+"EditDTO";
                objectMap.put("app_EditDto", appEditDto);
                String editDtoPathFile = String.format((editDtoPath + File.separator + "%s" +  suffixJavaOrKt()),appEditDto );
                outputFile(new File(editDtoPathFile), objectMap, editDto, true);
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getAppDtoPagelistDto).ifPresent((pageListDto) -> {
                String appPageListDto = entityA+"PageListDTO";
                objectMap.put("app_PageListDto", appPageListDto);
                String appPageListDtoFile = String.format((pageListDtoPath + File.separator + "%s" +  suffixJavaOrKt()),appPageListDto);
                outputFile(new File(appPageListDtoFile), objectMap, pageListDto,true);
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getAppVoPagelistVo).ifPresent((pageListVo) -> {
                String appPageListVo =  entityA+"PageListVO";
                objectMap.put("app_PageListVo", appPageListVo);
                String appPageListVoFile = String.format((pageListVoPath + File.separator + "%s" +  suffixJavaOrKt()),appPageListVo);
                outputFile(new File(appPageListVoFile), objectMap, pageListVo, true);
            });
            getMicroTemplateFilePath(MicroTemplateConfig::getAppService).ifPresent((iservice) -> {
                String iServiceName = tableInfo.getServiceName();
                objectMap.put("app_iServiceName", iServiceName);
                String iServiceNameFile = String.format((servicePath + File.separator + "%s" + suffixJavaOrKt()), iServiceName);
                outputFile(new File(iServiceNameFile), objectMap, iservice, true);
            });

            getMicroTemplateFilePath(MicroTemplateConfig::getAppController).ifPresent((rest) -> {
                String appController = entityA + "Controller";
                objectMap.put("app_Controller", appController);
                String controllerFile = String.format((restPath + File.separator + "%s" + suffixJavaOrKt()), appController);
                outputFile(new File(controllerFile), objectMap, rest, true);
            });


            getMicroTemplateFilePath(MicroTemplateConfig::getAppServiceImpl).ifPresent((serviceImpl) -> {
                String iServiceImplName = entityA+"ServiceImpl";
                objectMap.put("app_iServiceImplName", iServiceImplName);
                String iServiceImplNameFile = String.format((serviceImplPath + File.separator + "%s" +  suffixJavaOrKt()), iServiceImplName);
                outputFile(new File(iServiceImplNameFile), objectMap, serviceImpl, true);
            });
        }
    }




    /**
     * 输出实体文件
     *
     * @param tableInfo 表信息
     * @param objectMap 渲染数据
     * @since 3.5.0
     */
    protected void outputEntity(@NotNull TableInfo tableInfo, @NotNull Map<String, Object> objectMap) {
        String entityName = tableInfo.getEntityName();
        String entityPath = getPathInfo(OutputFile.entity);
        if (StringUtils.isNotBlank(entityName) && StringUtils.isNotBlank(entityPath)) {
            getTemplateFilePath(template -> template.getEntity(getConfigBuilder().getGlobalConfig().isKotlin())).ifPresent((entity) -> {
                String entityFile = String.format((entityPath + File.separator + "%s" + suffixJavaOrKt()), entityName);
                outputFile(new File(entityFile), objectMap, entity, getConfigBuilder().getStrategyConfig().entity().isFileOverride());
            });
        }
    }

    /**
     * 输出Mapper文件(含xml)
     *
     * @param tableInfo 表信息
     * @param objectMap 渲染数据
     * @since 3.5.0
     */
    protected void outputMapper(@NotNull TableInfo tableInfo, @NotNull Map<String, Object> objectMap) {
        // MpMapper.java
        String entityName = tableInfo.getEntityName();
        String mapperPath = getPathInfo(OutputFile.mapper);
        if (StringUtils.isNotBlank(tableInfo.getMapperName()) && StringUtils.isNotBlank(mapperPath)) {
            getTemplateFilePath(TemplateConfig::getMapper).ifPresent(mapper -> {
                String mapperFile = String.format((mapperPath + File.separator + tableInfo.getMapperName() + suffixJavaOrKt()), entityName);
                outputFile(new File(mapperFile), objectMap, mapper, getConfigBuilder().getStrategyConfig().mapper().isFileOverride());
            });
        }
        // MpMapper.xml
        String xmlPath = getPathInfo(OutputFile.xml);
        if (StringUtils.isNotBlank(tableInfo.getXmlName()) && StringUtils.isNotBlank(xmlPath)) {
            getTemplateFilePath(TemplateConfig::getXml).ifPresent(xml -> {
                String xmlFile = String.format((xmlPath + File.separator + tableInfo.getXmlName() + ConstVal.XML_SUFFIX), entityName);
                outputFile(new File(xmlFile), objectMap, xml, getConfigBuilder().getStrategyConfig().mapper().isFileOverride());
            });
        }
    }

    /**
     * 输出service文件
     *
     * @param tableInfo 表信息
     * @param objectMap 渲染数据
     * @since 3.5.0
     */
    protected void outputService(@NotNull TableInfo tableInfo, @NotNull Map<String, Object> objectMap) {
        // IMpService.java
        String entityName = tableInfo.getEntityName();
        String servicePath = getPathInfo(OutputFile.service);
        if (StringUtils.isNotBlank(tableInfo.getServiceName()) && StringUtils.isNotBlank(servicePath)) {
            getTemplateFilePath(TemplateConfig::getService).ifPresent(service -> {
                String serviceFile = String.format((servicePath + File.separator + tableInfo.getServiceName() + suffixJavaOrKt()), entityName);
                outputFile(new File(serviceFile), objectMap, service, getConfigBuilder().getStrategyConfig().service().isFileOverride());
            });
        }
        // MpServiceImpl.java
        String serviceImplPath = getPathInfo(OutputFile.serviceImpl);
        if (StringUtils.isNotBlank(tableInfo.getServiceImplName()) && StringUtils.isNotBlank(serviceImplPath)) {
            getTemplateFilePath(TemplateConfig::getServiceImpl).ifPresent(serviceImpl -> {
                String implFile = String.format((serviceImplPath + File.separator + tableInfo.getServiceImplName() + suffixJavaOrKt()), entityName);
                outputFile(new File(implFile), objectMap, serviceImpl, getConfigBuilder().getStrategyConfig().service().isFileOverride());
            });
        }
    }

    /**
     * 输出controller文件
     *
     * @param tableInfo 表信息
     * @param objectMap 渲染数据
     * @since 3.5.0
     */
    protected void outputController(@NotNull TableInfo tableInfo, @NotNull Map<String, Object> objectMap) {
        // MpController.java
        String controllerPath = getPathInfo(OutputFile.controller);
        if (StringUtils.isNotBlank(tableInfo.getControllerName()) && StringUtils.isNotBlank(controllerPath)) {
            getTemplateFilePath(TemplateConfig::getController).ifPresent(controller -> {
                String entityName = tableInfo.getEntityName();
                String controllerFile = String.format((controllerPath + File.separator + tableInfo.getControllerName() + suffixJavaOrKt()), entityName);
                outputFile(new File(controllerFile), objectMap, controller, getConfigBuilder().getStrategyConfig().controller().isFileOverride());
            });
        }
    }

    /**
     * 输出文件（3.5.3版本会删除此方法）
     *
     * @param file         文件
     * @param objectMap    渲染信息
     * @param templatePath 模板路径
     * @since 3.5.0
     */
    @Deprecated
    protected void outputFile(@NotNull File file, @NotNull Map<String, Object> objectMap, @NotNull String templatePath) {
        outputFile(file, objectMap, templatePath, false);
    }

    /**
     * 输出文件
     *
     * @param file         文件
     * @param objectMap    渲染信息
     * @param templatePath 模板路径
     * @param fileOverride 是否覆盖已有文件
     * @since 3.5.2
     */
    protected void outputFile(@NotNull File file, @NotNull Map<String, Object> objectMap, @NotNull String templatePath, boolean fileOverride) {
        if (isCreate(file, fileOverride)) {
            try {
                // 全局判断【默认】
                boolean exist = file.exists();
                if (!exist) {
                    File parentFile = file.getParentFile();
                    FileUtils.forceMkdir(parentFile);
                }
                writer(objectMap, templatePath, file);
            } catch (Exception exception) {
                throw new RuntimeException(exception);
            }
        }
    }

    /**
     * 获取模板路径
     *
     * @param function function
     * @return 模板路径
     * @since 3.5.0
     */
    @NotNull
    protected Optional<String> getTemplateFilePath(@NotNull Function<TemplateConfig, String> function) {
        TemplateConfig templateConfig = getConfigBuilder().getTemplateConfig();
        String filePath = function.apply(templateConfig);
        if (StringUtils.isNotBlank(filePath)) {
            return Optional.of(templateFilePath(filePath));
        }
        return Optional.empty();
    }

    /**
     * 获取路径信息
     *
     * @param outputFile 输出文件
     * @return 路径信息
     */
    @Nullable
    protected String getPathInfo(@NotNull OutputFile outputFile) {
        return getConfigBuilder().getPathInfo().get(outputFile);
    }


    /**
     * 获取模板路径
     *
     * @param function function
     * @return 模板路径
     * @since 3.5.0
     */
    @NotNull
    protected Optional<String> getMicroTemplateFilePath(@NotNull Function<MicroTemplateConfig, String> function) {
        MicroTemplateConfig templateConfig = getConfigBuilder().getMicroTemplateConfig();
        String filePath = function.apply(templateConfig);
        if (StringUtils.isNotBlank(filePath)) {
            return Optional.of(templateFilePath(filePath));
        }
        return Optional.empty();
    }

    /**
     * 获取路径信息
     *
     * @param outputFile 输出文件
     * @return 路径信息
     */
    @Nullable
    protected String getMicroPathInfo(@NotNull MicroOutputFile outputFile) {
        return getConfigBuilder().getMicroPathInfo().get(outputFile);
    }



    /**
     * 批量输出 java xml 文件
     */
    @NotNull
    public AbstractTemplateEngine batchOutput() {
        try {
            ConfigBuilder config = this.getConfigBuilder();
            List<TableInfo> tableInfoList = config.getTableInfoList();
            tableInfoList.forEach(tableInfo -> {
                Map<String, Object> objectMap = this.getObjectMap(config, tableInfo,false);
                Optional.ofNullable(config.getInjectionConfig()).ifPresent(t -> {
                    t.beforeOutputFile(tableInfo, objectMap);
                    // 输出自定义文件
                    outputCustomFile(t.getCustomFile(), tableInfo, objectMap);
                });
                // entity
                outputEntity(tableInfo, objectMap);
                // mapper and xml
                outputMapper(tableInfo, objectMap);
                // service
                outputService(tableInfo, objectMap);
                // controller
                outputController(tableInfo, objectMap);
            });
        } catch (Exception e) {
            throw new RuntimeException("无法创建文件，请检查配置信息！", e);
        }
        return this;
    }

    /**
     * 批量输出 java xml 文件
     */
    @NotNull
    public AbstractTemplateEngine batchOutputMicro() {
        try {
            ConfigBuilder config = this.getConfigBuilder();
            List<TableInfo> tableInfoList = config.getTableInfoList();
            tableInfoList.forEach(tableInfo -> {
                //得到对象类型
                Map<String, Object> objectMap = this.getObjectMap(config, tableInfo,true);
                Optional.ofNullable(config.getInjectionConfig()).ifPresent(t -> {
                    t.beforeOutputFile(tableInfo, objectMap);
                    // 输出自定义文件
                    outputCustomFile(t.getCustomFile(), tableInfo, objectMap);
                });
                //TODO
                // client
                outputClient(tableInfo, objectMap);
                // domian
                outputDomain(tableInfo, objectMap);
                // app
                outputApp(tableInfo, objectMap);
            });
        } catch (Exception e) {
            throw new RuntimeException("无法创建文件，请检查配置信息！", e);
        }
        return this;
    }


    /**
     * 输出文件（3.5.3版本会删除此方法）
     *
     * @param objectMap    渲染数据
     * @param templatePath 模板路径
     * @param outputFile   输出文件
     * @throws Exception ex
     * @deprecated 3.5.0
     */
    @Deprecated
    protected void writerFile(Map<String, Object> objectMap, String templatePath, String outputFile) throws Exception {
        if (StringUtils.isNotBlank(templatePath)) {this.writer(objectMap, templatePath, outputFile);}
    }

    /**
     * 将模板转化成为文件（3.5.3版本会删除此方法）
     *
     * @param objectMap    渲染对象 MAP 信息
     * @param templatePath 模板文件
     * @param outputFile   文件生成的目录
     * @see #writer(Map, String, File)
     * @deprecated 3.5.0
     */
    @Deprecated
    public void writer(@NotNull Map<String, Object> objectMap, @NotNull String templatePath, @NotNull String outputFile) throws Exception {

    }

    /**
     * 将模板转化成为文件
     *
     * @param objectMap    渲染对象 MAP 信息
     * @param templatePath 模板文件
     * @param outputFile   文件生成的目录
     * @throws Exception 异常
     * @since 3.5.0
     */
    public void writer(@NotNull Map<String, Object> objectMap, @NotNull String templatePath, @NotNull File outputFile) throws Exception {
        this.writer(objectMap, templatePath, outputFile.getPath());
        logger.debug("模板:" + templatePath + ";  文件:" + outputFile);
    }

    /**
     * 打开输出目录
     */
    public void open() {
        String outDir = getConfigBuilder().getGlobalConfig().getOutputDir();
        if (StringUtils.isBlank(outDir) || !new File(outDir).exists()) {
            System.err.println("未找到输出目录：" + outDir);
        } else if (getConfigBuilder().getGlobalConfig().isOpen()) {
            try {
                RuntimeUtils.openDir(outDir);
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    /**
     * 渲染对象 MAP 信息
     *
     * @param config    配置信息
     * @param tableInfo 表信息对象
     * @return ignore
     */
    @NotNull
    public Map<String, Object> getObjectMap(@NotNull ConfigBuilder config, @NotNull TableInfo tableInfo,boolean isMicro) {
        StrategyConfig strategyConfig = config.getStrategyConfig();
        Map<String, Object> controllerData = strategyConfig.controller().renderData(tableInfo);
        Map<String, Object> objectMap = new HashMap<>(controllerData);
        Map<String, Object> mapperData = strategyConfig.mapper().renderData(tableInfo);
        objectMap.putAll(mapperData);
        Map<String, Object> serviceData = strategyConfig.service().renderData(tableInfo);
        objectMap.putAll(serviceData);
        Map<String, Object> entityData = strategyConfig.entity().renderData(tableInfo);
        objectMap.putAll(entityData);
        objectMap.put("config", config);
        String entityA = tableInfo.getEntityClass();
        String entitya = entityA.substring(0, 1).toLowerCase() + entityA.substring(1);
        if (isMicro) {
            objectMap.put("entity_A",entityA);
            objectMap.put("entity_a",entitya );
            objectMap.put("serviceImplName", entitya + "Service");
            objectMap.put("package", config.getMicroPackageConfig().getPackageInfo());
        }else{
            objectMap.put("package", config.getPackageConfig().getPackageInfo());
        }
        GlobalConfig globalConfig = config.getGlobalConfig();
        objectMap.put("author", globalConfig.getAuthor());
        objectMap.put("kotlin", globalConfig.isKotlin());
        objectMap.put("swagger", globalConfig.isSwagger());
        objectMap.put("date", globalConfig.getCommentDate());
        // 启用 schema 处理逻辑
        String schemaName = "";
        if (strategyConfig.isEnableSchema()) {
            // 存在 schemaName 设置拼接 . 组合表名
            schemaName = config.getDataSourceConfig().getSchemaName();
            if (StringUtils.isNotBlank(schemaName)) {
                schemaName += ".";
                tableInfo.setConvert(true);
            }
        }
        objectMap.put("schemaName", schemaName);
        objectMap.put("table", tableInfo);
        objectMap.put("entity", tableInfo.getEntityName());
        return objectMap;
    }

    public String getFirstLowerCase(@NotNull String name) {
        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }

    /**
     * 模板真实文件路径
     *
     * @param filePath 文件路径
     * @return ignore
     */
    @NotNull
    public abstract String templateFilePath(@NotNull String filePath);

    /**
     * 检测文件是否存在（3.5.3版本会删除此方法）
     *
     * @return 文件是否存在
     * @deprecated 3.5.0
     */
    @Deprecated
    protected boolean isCreate(String filePath) {
        return isCreate(new File(filePath));
    }

    /**
     * 检查文件是否创建文件（3.5.3版本会删除此方法）
     *
     * @param file 文件
     * @return 是否创建文件
     * @since 3.5.0
     */
    @Deprecated
    protected boolean isCreate(@NotNull File file) {
        // 全局判断【默认】
        return !file.exists() || getConfigBuilder().getGlobalConfig().isFileOverride();
    }

    /**
     * 检查文件是否创建文件
     *
     * @param file         文件
     * @param fileOverride 是否覆盖已有文件
     * @return 是否创建文件
     * @since 3.5.2
     */
    protected boolean isCreate(@NotNull File file, boolean fileOverride) {
        // 全局判断【默认】
        return !file.exists() || fileOverride;
    }

    /**
     * 文件后缀
     */
    protected String suffixJavaOrKt() {
        return getConfigBuilder().getGlobalConfig().isKotlin() ? ConstVal.KT_SUFFIX : ConstVal.JAVA_SUFFIX;
    }

    @NotNull
    public ConfigBuilder getConfigBuilder() {
        return configBuilder;
    }

    @NotNull
    public AbstractTemplateEngine setConfigBuilder(@NotNull ConfigBuilder configBuilder) {
        this.configBuilder = configBuilder;
        return this;
    }
}
