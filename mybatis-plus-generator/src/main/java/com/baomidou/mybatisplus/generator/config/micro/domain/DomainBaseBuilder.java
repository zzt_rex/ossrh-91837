package com.baomidou.mybatisplus.generator.config.micro.domain;

import com.baomidou.mybatisplus.generator.config.IConfigBuilder;
import org.jetbrains.annotations.NotNull;

public class DomainBaseBuilder implements IConfigBuilder<DomainPackageConfig> {
    private final DomainPackageConfig domainPackageConfig;

    public DomainBaseBuilder(@NotNull DomainPackageConfig domainPackageConfig) {
        this.domainPackageConfig = domainPackageConfig;
    }

    @NotNull
    @Override
    public DomainPackageConfig build() {
        return this.domainPackageConfig;
    }



}
