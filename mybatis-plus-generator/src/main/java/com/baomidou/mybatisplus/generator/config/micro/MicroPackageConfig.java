/*
 * Copyright (c) 2011-2021, baomidou (jobob@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.baomidou.mybatisplus.generator.config.micro;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.config.micro.app.AppPackageConfig;
import com.baomidou.mybatisplus.generator.config.micro.client.ClientPackageConfig;
import com.baomidou.mybatisplus.generator.config.micro.domain.DomainPackageConfig;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 微服务包相关的配置项
 *
 * @author lujun
 */
public class MicroPackageConfig {

    private MicroPackageConfig() {
    }

    /**
     * 父包名。如果为空，将下面子包名必须写全部， 否则就只需写子包名
     */
    private String parent = "com.jshcbd";

    /**
     * 总的父包模块名
     */
    private String moduleName = "";

    private final DomainPackageConfig.Builder domainPackageConfigBuilder = new DomainPackageConfig.Builder(this);

    private final ClientPackageConfig.Builder clientPackageConfigBuilder = new ClientPackageConfig.Builder(this);

    private final AppPackageConfig.Builder appPackageConfigBuilder = new AppPackageConfig.Builder(this);

    public DomainPackageConfig.Builder domainPackageConfigBuilder(){
        return this.domainPackageConfigBuilder;
    }
    public ClientPackageConfig.Builder clientPackageConfigBuilder(){
        return this.clientPackageConfigBuilder;
    }
    public AppPackageConfig.Builder appPackageConfigBuilder(){
        return this.appPackageConfigBuilder;
    }

    /**
     * 路径配置信息
     */
    private Map<MicroOutputFile, String> pathInfo;


    /**
     * 包配置信息
     *
     * @since 3.5.0
     */
    private final Map<String, String> packageInfo = new HashMap<>();

    /**
     * 父包名
     */
    @NotNull
    public String getParent() {
        if (StringUtils.isNotBlank(moduleName)) {
            return parent + StringPool.DOT + moduleName;
        }
        return parent;
    }


    /**
     * 根项目包路径
     * @return
     */
    @NotNull
    public String getRoot() {
        return parent;
    }


    /**
     * 连接父子包名
     *
     * @param subPackage 子包名
     * @return 连接后的包名
     */
    @NotNull
    public String joinPackage(String subPackage) {
        String parent = getParent();
        return StringUtils.isBlank(parent) ? subPackage : (parent + StringPool.DOT + subPackage);
    }

    private void joinPackage(Map<String,String> parentMap,Map<String,String> subPackageInfo){
        if (CollectionUtils.isNotEmpty(subPackageInfo)
            && StringUtils.isNotBlank(getParent())) {
            for (String key : subPackageInfo.keySet()) {
                String pageckageName = subPackageInfo.get(key);
                if (StringUtils.isNotBlank(pageckageName)) {
                    parentMap.put(key, getParent() + StringPool.DOT + pageckageName);
                }
            }
        }
    }

    /**
     * 获取包配置信息
     *
     * @return 包配置信息
     * @since 3.5.0
     */
    @NotNull
    public Map<String, String> getPackageInfo() {
        if (packageInfo.isEmpty()) {
            packageInfo.put(MicroConstVal.MODULE_NAME, this.getModuleName());
            packageInfo.put(MicroConstVal.PARENT, this.getParent());
            packageInfo.put(MicroConstVal.ROOT, this.getRoot());
            joinPackage(packageInfo, domainPackageConfigBuilder.get().getPackageInfo());
            joinPackage(packageInfo, clientPackageConfigBuilder.get().getPackageInfo());
            joinPackage(packageInfo, appPackageConfigBuilder.get().getPackageInfo());
        }
        return Collections.unmodifiableMap(this.packageInfo);
    }

    public Map<MicroOutputFile, String> getPathInfo() {
        return pathInfo;
    }

    /**
     * 获取包配置信息
     *
     * @param module 模块
     * @return 配置信息
     * @since 3.5.0
     */
    public String getPackageInfo(String module) {
        return getPackageInfo().get(module);
    }

    public String getModuleName() {
        return moduleName;
    }

    /**
     * 构建者
     *
     * @author nieqiurong
     * @since 3.5.0
     */
    public static class Builder extends MicroBaseBuilder {

        private final MicroPackageConfig microPackageConfig;

        public Builder() {
            super(new MicroPackageConfig());
            this.microPackageConfig = super.build();
        }

        public Builder(@NotNull String parent, @NotNull String moduleName) {
            this();
            this.microPackageConfig.parent = parent;
            this.microPackageConfig.moduleName = moduleName;
        }

        /**
         * 指定父包名
         *
         * @param parent 父包名
         * @return this
         */
        public Builder parent(@NotNull String parent) {
            this.microPackageConfig.parent = parent;
            return this;
        }

        /**
         * 指定模块名称
         *
         * @param moduleName 模块名
         * @return this
         */
        public Builder moduleName(@NotNull String moduleName) {
            this.microPackageConfig.moduleName = moduleName;
            return this;
        }


        /**
         * 构建包配置对象
         * @return 包配置对象
         */
        @Override
        public @NotNull MicroPackageConfig build() {
            return this.microPackageConfig;
        }


    }
}
